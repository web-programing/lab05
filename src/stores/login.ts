import { ref, computed } from "vue";
import { defineStore } from "pinia";

export const useLoginStore = defineStore("login", () => {
  const loginName = ref("");
  const isLogin = computed(() => {
    // if loginName is not empty
    return loginName.value !== "";
  });
  //emits login ส่ง userName กลับมา
  const login = (userName: string): void => {
    loginName.value = userName;
    //เมื่อเรา login ได้เราควรนำไปเก็บใน local storaged
    localStorage.setItem("loginName", userName); //เก็บ userName ไว้ใน key ที่มีชื่อว่า loginName
  };
  const logout = () => {
    loginName.value = "";
    localStorage.removeItem("loginName");
  };
  const loadData = () => {
    loginName.value = localStorage.getItem("loginName") || "";
  };

  return { loginName, isLogin, login, logout, loadData };
});
